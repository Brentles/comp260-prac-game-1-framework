﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float maxSpeed = 5.0f; 
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");
        //input
        Vector2 velocity = direction * maxSpeed;
        transform.Translate(velocity * Time.deltaTime);

    }
}
