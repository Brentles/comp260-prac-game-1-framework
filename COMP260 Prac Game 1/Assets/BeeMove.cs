﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // Use this for initialization
    public float speed = 4.0f;
    public float turnSpeed = 180.0f; 
    public Transform target;
    public Vector2 heading = Vector3.right;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    // direction 
        Vector2 direction = target.position - transform.position;
    // heading
        float angle = turnSpeed * Time.deltaTime;
        if (direction.IsOnLeft(heading)) {
            heading = heading.Rotate(angle);
        } else {
            heading = heading.Rotate(-angle);
        }
    //velocity settings
        transform.Translate(heading * speed * Time.deltaTime);
	}
}
